from flask import render_template
from forms import CodeForm
from functions import get_res
from config import Config, TIMEOUT
# from app import app

from flask import Flask


app = Flask(__name__)
app.config.from_object(Config)


@app.route('/', methods=['GET', 'POST'])
def launcher():
    form = CodeForm()

    if form.validate_on_submit():
        print('valid')
        pycode = form.code.data
        inp = form.input.data

        timeout = form.float_time.data
        if timeout is None:
            timeout = TIMEOUT

        out, err = get_res(pycode, inp, timeout)

        form.output.data = out.getvalue()
        form.error.data = err.getvalue()
        return render_template("answer.html", title='Home', form=form)
    else:
        form.error.data = ''
        form.output.data = ''
        for key, val in form.errors.items():
            form.error.data += val[0] + '\n'
        #     flash(val)
        print('not valid')

    return render_template("answer.html", title='Home', form=form)


@app.route('/task2', methods=['GET', 'POST'])
def task1():
    form = CodeForm()

    if form.validate_on_submit():
        print('valid')
        pycode = form.code.data
        inp = form.input.data

        timeout = form.float_time.data
        if timeout is None or timeout > TIMEOUT:
            timeout = TIMEOUT

        out, err = get_res(pycode, inp, timeout)

        form.output.data = out.getvalue()
        form.error.data = err.getvalue()
        return render_template("answer.html", title='Launcher2', form=form)
    else:
        form.error.data = ''
        for key, val in form.errors.items():
            form.error.data += val + '\n'
        #     flash(val)
        print('not valid')

    return render_template("answer.html", title='Launcher2', form=form)


if __name__ == "__main__":
    app.run(host='0.0.0.0')
