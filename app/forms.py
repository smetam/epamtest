from flask_wtf import FlaskForm
from wtforms import TextAreaField, FloatField
from wtforms import validators
from config import TIMEOUT


#  main web form
class CodeForm(FlaskForm):
    def limit_validator(form, field):
        if field.data is None:
            raise validators.ValidationError(
                f'Timeout must be less than {TIMEOUT} seconds')
        elif float(field.data) > TIMEOUT:
            raise validators.ValidationError(
                f'Timeout must be less than {TIMEOUT} seconds')

    code = TextAreaField('Code', default='print("hi")',
                         validators=[validators.DataRequired(
                             message='Code field is required')])
    input = TextAreaField('Input', validators=[])
    output = TextAreaField('Output', validators=[])
    error = TextAreaField('Error', validators=[])
    float_time = FloatField('Timeout', validators=[limit_validator],
                            default=TIMEOUT)
