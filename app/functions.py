import io
import sys
import multiprocessing as mp


# function for user code
def func():
    print('hi')


def stream_open(filename, mode):
    if mode == 'w':
        file = io.StringIO()
        globals()[filename] = file
    elif mode == 'r':
        if filename in globals():
            return globals()[filename]
        else:
            raise FileNotFoundError


# target function for process
def executer(inp, return_dict):
    sys.stdin = io.StringIO(inp)
    result = io.StringIO()
    error = io.StringIO()
    sys.stderr = error
    sys.stdout = result
    temp_open = globals()['__builtins__']['open']
    globals()['__builtins__']['open'] = stream_open
    try:
        func()
    except Exception as e:
        error.write(str(e))

    return_dict['result'] = result
    return_dict['error'] = error
    globals()['__builtins__']['open'] = temp_open
    sys.stdin, sys.stdout, sys.stderr = \
        sys.__stdin__, sys.__stdout__, sys.__stderr__


# Custom exception for Timeout
class TimeLimitExpired(Exception):
    def __str__(self):
        return "TimeLimitExpired"


# results of user code execution
def get_res(code, inp, timeout):
    manager = mp.Manager()
    return_dict = manager.dict()

    try:
        func.__code__ = compile(code, '', 'exec')
        proc = mp.Process(target=executer, args=(inp, return_dict))
        proc.start()
        proc.join(timeout=timeout)

        if proc.is_alive():
            proc.terminate()
            proc.join()
            sys.stdin, sys.stdout, sys.stderr = \
                sys.__stdin__, sys.__stdout__, sys.__stderr__
            raise TimeLimitExpired
        else:
            result = return_dict['result']
            error = return_dict['error']

    except Exception as e:
        result = io.StringIO('')
        error = io.StringIO()
        error.write(str(e))

    return result, error
